Object.defineProperty(window, "mainConfig", {get:function(){
    return {
        'Users' : {
            'apiSource' : 'users/',
            'templatePath' : 'Users/users.html'
        },
        'Companies' : {
            'apiSource' : 'companies/',
            'templatePath' : 'Companies/companies.html'
        },
        'TransferLogs' : {
            'apiSource' : 'report/',
            'templatePath' : 'TransferLogs/transferLogs.html'
        },
        'DataGenerator' : {
            'apiSource' : 'generator/',
            'templatePath' : 'Users/users.html'                            
        }
    };
}});



Object.defineProperty(window, "information_unit", {get:function(){
    return ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB'];
}});

Object.defineProperty(window, "API", {get:function(){
    return '/api/';
}});

if (!Object.keys) {
    Object.keys = (function () {
        'use strict';
        var hasOwnProperty = Object.prototype.hasOwnProperty,
        hasDontEnumBug = !({toString: null}).propertyIsEnumerable('toString'),
        dontEnums = [
            'toString',
            'toLocaleString',
            'valueOf',
            'hasOwnProperty',
            'isPrototypeOf',
            'propertyIsEnumerable',
            'constructor'
        ],
        
        dontEnumsLength = dontEnums.length;

        return function (obj) {
            if (typeof obj !== 'object' && (typeof obj !== 'function' || obj === null)) {
                throw new TypeError('Object.keys called on non-object');
            }

            var result = [], prop, i;

            for (prop in obj) {
                if (hasOwnProperty.call(obj, prop)) {
                    result.push(prop);
                }
            }

            if (hasDontEnumBug) {
                for (i = 0; i < dontEnumsLength; i++) {
                    if (hasOwnProperty.call(obj, dontEnums[i])) {
                        result.push(dontEnums[i]);
                    }
                }
            }
    
            return result;
        };
    }());
}

Object.defineProperty(window, "months", {get:function(){
    var month = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    ];
    return month;
}});