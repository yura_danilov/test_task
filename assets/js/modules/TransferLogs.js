Core.modules.TransferLogs = {
    report: [],
    init: function(){
    },

    run: function(){
        var _self = this;
        
        Core.modules.Helper.popup('show');
        _self.buildControls();
        Core.modules.Helper.popup();
    }, 

    buildControls: function()
    {
        $.each(months, function(index, month){
            $('#monthspicker').append(
                $("<option></option>")
                    .attr("value", index + 1)
                    .text(month));
        });
    },

    buildHeader: function(){
        var _self = this;
        
        $('#transfer_logs_table').load(Core.templatePath + 'TransferLogs/TransferLogsHeader.html', function(){
        });		
    },

    getReport: function(){
        var _self = this;
        
        var req = Core.modules.Helper.ajax('GET', mainConfig[Core.moduleName].apiSource + $('#monthspicker').find('option:selected').val());
        req.done(function(data){
            _self.buildReportForm(data);
        });
        req.fail(function(error){
        });		
    },

    buildReportForm: function(reportData)
    {
        var _self = this;
        
        $('#transfer_logs_table').empty();
        
        $.get(Core.templatePath + mainConfig[Core.moduleName].templatePath, function(template){
            if( reportData.length > 0 && typeof reportData == "object")
            {
                _self.buildHeader();
                
                $.each(reportData, function(index, report){
                    var clone = $(template).clone( true );
                    var id = $(clone).attr('id') + index;
                    
                    $('#transfer_logs_table').append(clone);
                    $("#transfer_logs_table > #report_number_").attr('id', id);
                    
                    var over_quota = Core.modules.Helper.formatBytes(parseInt(report.over_quota));
                    var quota = Core.modules.Helper.formatBytes(parseInt(report.quota));
  
                    $('#' + id).find('span[class="name"]').text(report.name);
                    $('#' + id).find('span[class="quota"]').text(over_quota + ' (' + quota + ')');
                });
                
            } else {
                $('#transfer_logs_table').append('No data');
            }
        });
    }
};
