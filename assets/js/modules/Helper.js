Core.modules.Helper = {
    ajax: function(type, url, data){
        $('body').addClass('loading');
        var _self = this;
        return $.ajax({
            url: API + url,
            type: type,
            data: data || '',
            async: false,
            cache: false,
            global: true,
            beforeSend: function () {
                Core.modules.Helper.overlay('show');
		return true;
            },
            complete: function(){
                Core.modules.Helper.overlay();
            }			
        }).always(function(){
            _self.overlay();
        });
    },
    
    overlay: function(action){
        if ('show' == action){
            $('body').addClass('loading');
        }
        else{
            $('body').removeClass('loading');
        }
    },

    formatBytes: function(bytes, decimals) {
        if(bytes == 0) return '0 Byte';
        var k = 1024;
        var dm = decimals + 1 || 2;
        var i = Math.floor(Math.log(bytes) / Math.log(k));
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + information_unit[i];
    },
    
    convertToBytes: function(quota_size, quota_type){
        var quota = 0;
        var k = 1024;
        $.each(information_unit, function(index, type){
            if( quota_type == type)
            {
                quota = parseInt(quota_size * Math.pow(k, index));
            }
        });
        
        return quota;
    },

    closeDialog: function(){
        Core.modules.Helper.popup('close');
        $('#addDialog').remove();
    },

    showDialog: function(title, message, flag){
        var dialog = $('<div/>', {
                id: 'dialog-message'
            }).append(message).dialog({
                modal: true,
                title: title,
                dialogClass: 'grt_custom_dialog',
                beforeClose: function(){
                        if (flag){
                                Core.modules.Helper.popup('close');
                        }
                },
                buttons: {
                        Ok: function(){
                                $(this).dialog('close');
                        }
                }
            });
    },

    popup: function(action, zIndex){
        if('number' == typeof parseInt(zIndex)){
            $('#opaco').css('z-index',zIndex);
        }
        if ('show' == action){
            $('#opaco').height($(document).height()).toggleClass('hidden');
        }
        else{
            $('#opaco').toggleClass('hidden').removeAttr('style');
            $('#popup').html();
            $('#popup').toggleClass('hidden');
        }
    }
};
