Core.modules.DataGenerator = {
    companies: [],
    init: function(){
        
    },

    run: function(){
        var _self = this;
        Core.modules.Helper.popup('show', 1000);
        
        var req = Core.modules.Helper.ajax('POST', mainConfig['DataGenerator'].apiSource);
        req.done(function(msg){
            Core.modules.Helper.showDialog('Data generator', 'Data generated', 1);
        });
        req.fail(function(error){
           Core.modules.Helper.showDialog('Data generator', error, 1);
        });
    }
};
