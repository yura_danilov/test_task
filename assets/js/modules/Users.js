Core.modules.Users = {
    companies: [],
    users:[],
    
    init: function(){
    },

    run: function(){
        var _self = this;
        
        Core.modules.Helper.popup('show');
        _self.buildUsersForm();
        Core.modules.Helper.popup();
    },

    buildHeader: function(){
        var _self = this;
        
        $('#users_table').load(Core.templatePath + 'Users/usersHeader.html', function(){
        });		
    },

    getUsers: function(){
        var _self = this;

        var req = Core.modules.Helper.ajax('GET', mainConfig[Core.moduleName].apiSource);
        req.done(function(data){
            _self.users = data;
        });
        req.fail(function(error){
            _self.users = error;
        });
        
        return _self.users;
    },

    buildUsersForm: function()
    {
        var _self = this;

        _self.companies = Core.modules.Companies.getCompanies();
        _self.getUsers();
        $('#users_table').empty();
        
        $.get(Core.templatePath + mainConfig[Core.moduleName].templatePath, function(template){
            if( _self.users.length > 0 && typeof _self.users == "object")
            {
                _self.buildHeader();
                
                $.each(_self.users, function(index, user){
                    var clone = $(template).clone( true );
                    var id = $(clone).attr('id') + index;
                    
                    $('#users_table').append(clone);
                    $("#users_table > #user_number_").attr('id', id);
                    
                    $('#' + id).find('input[type="hidden"][name="id"]').val(user.id);
                    $('#' + id).find('input[type="text"][name="name"]').val(user.name);
                    $('#' + id).find('input[type="text"][name="email"]').val(user.email);
                    $('#' + id).find('input[type="text"][name="company"]').val(user.company);
                    
                    $('#' + id).find('span[class="name"]').text(user.name);
                    $('#' + id).find('span[class="email"]').text(user.email);
                    $('#' + id).find('span[class="company"]').text(user.company);
                    
                    $.each(_self.companies, function(ind, company){
                        if( user.company == company.name )
                        {
                            $('#' + id).find('select[name="company"]')
                                .append($("<option></option>")
                                    .attr("value",company.id)
                                    .text(company.name)
                                    .prop('selected', true));
                        } else {
                            $('#' + id).find('select[name="company"]')
                                .append($("<option></option>")
                                    .attr("value",company.id)
                                    .text(company.name));
                        }
                    });
                });
                
            } else {
                $('#users_table').append('No users');
            }
        });
    },

    editUser: function(element)
    {
        var parent_id = '#' + $(element).parent()[0].id;


        $(parent_id + ' .users_lables').hide();
        $(parent_id + ' .user_edit_buttons').hide();
        $(parent_id + ' .users_inputs').show();
        $(parent_id + ' .user_buttons').show();
    },

    cancleEdit: function(element)
    {
        var parent_id = '#' + $(element).parent().parent()[0].id;

        $(parent_id + ' .users_lables').show();
        $(parent_id + ' .user_edit_buttons').show();
        $(parent_id + ' .users_inputs').hide();
        $(parent_id + ' .user_buttons').hide();
    },

    saveUser: function(element)
    {	
        var _self = this;
        var parent_id = '#' + $(element).parent().parent()[0].id;

        $(parent_id + ' .form-control').tooltipster({
            trigger: 'custom',
            onlyOne: false,
            position: 'right',
            theme: 'tooltipster-shadow'
        });
        
        $(parent_id + ' form').validate({
            errorPlacement: function (error, element) {
                $(element).tooltipster('update', $(error).text());
                $(element).tooltipster('show');
            },
            success: function (label, element) {
                $(element).tooltipster('hide');
            },
            rules: {
                name: {
                    required: true,
                    minlength: 2,
                    maxlength: 20,
                },
                email: {
                    required: true,
                    email: true
                }
            }
        });

        if($(parent_id + ' form').valid())
        {
            var user_id = $(parent_id).find('input[type="hidden"][name="id"]').val();
            var data = {
                'user_id':      $(parent_id).find('input[type="hidden"][name="id"]').val(),
                'name':         $(parent_id + ' form').find('input[type="text"][name="name"]').val(),
                'email':        $(parent_id + ' form').find('input[type="text"][name="email"]').val(),
                'company_id':   $(parent_id + ' form').find('select[name="company"]').val()
            }
            
            Core.modules.Helper.popup('show');
            
            var req = Core.modules.Helper.ajax('PUT', mainConfig[Core.moduleName].apiSource + user_id, JSON.stringify(data));
            req.done(function(msg){
                Core.modules.Helper.showDialog('Users', 'User edit successfully', 1);
                _self.run();
            });
            req.fail(function(msg){
                Core.modules.Helper.popup('show');
                Core.modules.Helper.showDialog('Users', msg.responseJSON.errors.message, 1);	
            });
        }
    },

    deleteUser: function(element){
        var _self = this;
        var parent_id = '#' + $(element).parent().parent()[0].id;
        var user_id = $(parent_id).find('input[type="hidden"][name="id"]').val();

        var req = Core.modules.Helper.ajax('DELETE', mainConfig[Core.moduleName].apiSource + user_id);
        req.done(function(){
            _self.run();
        });
        req.fail(function(error){
        });
    },

    prepareDialog: function(){
        var _self = this;
        var formDiv = $('<div/>', {id : 'addDialog', style: 'display: none', title: 'ADD USER'});
        formDiv.load(Core.templatePath + 'Users/addUser.html', function(){

        });
        $('#dialogBox').append(formDiv);

        if( _self.companies.length > 0 && typeof _self.companies == "object")
        {
            $.each(_self.companies, function(ind, company){
                $('.bootstrap-frm').find('select[name="company"]')
                    .append($("<option></option>")
                        .attr("value",company.id)
                        .text(company.name));
            });
        }
        else
        {   
            formDiv.remove();
            Core.modules.Helper.showDialog('Users', 'Please create company first', 1);
            return;
        }

        $( "#addDialog" ).dialog({
            width: 330,
            height: 345,
            position: { at: "top top+10%", of: window},
            open: function()
            {
                Core.modules.Helper.popup('show');
            },
            close: function(event, ui)
            {
                $('#addDialog').remove();
            },
            beforeClose: function() {Core.modules.Helper.popup('close');}
        });
        
        $('#opaco').click(function(){
            Core.modules.Helper.closeDialog();
        });
    },

    addUser: function(){
        var _self = this;
        $('.bootstrap-frm input[type="text"]').tooltipster({
            trigger: 'custom',
            onlyOne: false,
            position: 'right',
            theme: 'tooltipster-shadow'
        });
        $(".bootstrap-frm").validate({
            errorPlacement: function (error, element) {
                $(element).tooltipster('update', $(error).text());
                $(element).tooltipster('show');
            },
            success: function (label, element) {
                $(element).tooltipster('hide');
            },
            rules: {
                name: {
                    required: true,
                    minlength: 2,
                    maxlength: 20,
                },
                email: {
                    required: true,
                    email: true
                }
            }
        });

        if($(".bootstrap-frm").valid()){
            var name =          $(".bootstrap-frm input[name='name']").val();
            var email =         $(".bootstrap-frm input[name='email']").val();
            var company_id =    $(".bootstrap-frm select[name='company'] option:selected").val();

            var data = {'name': name, 'email': email, 'company_id': company_id};

            var req = Core.modules.Helper.ajax('POST',  mainConfig[Core.moduleName].apiSource, JSON.stringify(data));
            req.done(function(msg){
                Core.modules.Helper.showDialog('Users', 'User create successfully', 1);
                Core.modules.Helper.closeDialog();
                _self.run();
            });
            req.fail(function(msg){
                Core.modules.Helper.popup('show');
                Core.modules.Helper.showDialog('Users', msg.responseJSON.errors.message, 1);
            });
        }
    }
};
