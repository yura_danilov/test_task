Core.modules.Companies = {
    companies: [],
    init: function(){
    },
    
    run: function(){
        var _self = this;
        
        Core.modules.Helper.popup('show');
        _self.buildCompaniesForm();
        Core.modules.Helper.popup();
    },

    buildHeader: function(){
        var _self = this;
        
        $('#companies_table').load(Core.templatePath + 'Companies/companiesHeader.html', function(){
        });		
    },

    bindEvents: function(){
        var _self = this;
        
        $('.cancel').click(function(){
            $(this).parent().hide();
            $(this).parent().find('input[type="text"].tooltipstered.error').tooltipster('hide');
        });
    },
    
    getCompanies: function(){
        var _self = this;
        
        var req = Core.modules.Helper.ajax('GET', mainConfig['Companies'].apiSource);
        req.done(function(data){
            _self.companies = data;
        });
        req.fail(function(error){
            _self.companies =  error;
        });	
        
        return _self.companies;
    },

    buildCompaniesForm: function()
    {
        var _self = this;
        
        _self.getCompanies();
        $('#companies_table').empty();
       
        $.get(Core.templatePath + mainConfig[Core.moduleName].templatePath, function(template){
            if( _self.companies.length > 0 && typeof _self.companies == "object")
            {
                _self.buildHeader();
                
                $.each(_self.companies, function(index, company){
                    var clone = $(template).clone( true );
                    var id = $(clone).attr('id') + index;
  
                    $('#companies_table').append(clone);
                    $("#companies_table > #company_number_").attr('id', id);
                    
                    var qouta_type = Core.modules.Helper.formatBytes(company.quota);
                    var quota_size = qouta_type.split(' ')[0];
                    var qtuta_type = qouta_type.split(' ')[1];
                    
                    $('#companies_table > #' + id).find('input[type="hidden"][name="id"]').val(company.id);
                    $('#companies_table > #' + id).find('input[type="text"][name="name"]').val(company.name);
                    $('#companies_table > #' + id).find('input[type="text"][name="quota"]').val(quota_size);
                    
                    $('#companies_table > #' + id).find('span[class="name"]').text(company.name);
                    $('#companies_table > #' + id).find('span[class="quota"]').text(quota_size);    
                    $('#companies_table > #' + id).find('span[class="qtuta_type"]').text(qtuta_type); 
                    
                    $.each(information_unit, function(ind, type){
                        if( qtuta_type == type )
                        {
                            $('#' + id).find('select[name="quota_type"]')
                                .append($("<option></option>")
                                    .attr("value",ind)
                                    .text(type)
                                    .prop('selected', true));
                        } else {
                            $('#' + id).find('select[name="quota_type"]')
                                .append($("<option></option>")
                                    .attr("value",ind)
                                    .text(type));
                        }
                    });
                });
                
            } else {
                $('#companies_table').append('No Companies');
            }
        });
    },

    editCompany: function(element)
    {
        var parent_id = '#' + $(element).parent()[0].id;

        $(parent_id + ' .users_lables').hide();
        $(parent_id + ' .user_edit_buttons').hide();
        $(parent_id + ' .users_inputs').show();
        $(parent_id + ' .user_buttons').show();
    },

    cancleEdit: function(element)
    {
        var parent_id = '#' + $(element).parent().parent()[0].id;

        $(parent_id + ' .users_lables').show();
        $(parent_id + ' .user_edit_buttons').show();
        $(parent_id + ' .users_inputs').hide();
        $(parent_id + ' .user_buttons').hide();
    },

    saveCompany: function(element)
    {	
        var _self = this;
        var parent_id = '#' + $(element).parent().parent()[0].id;

        $(parent_id + ' .form-control').tooltipster({
            trigger: 'custom',
            onlyOne: false,
            position: 'right',
            theme: 'tooltipster-shadow'
        });
        
        $(parent_id + ' form').validate({
            errorPlacement: function (error, element) {
                $(element).tooltipster('update', $(error).text());
                $(element).tooltipster('show');
            },
            success: function (label, element) {
                $(element).tooltipster('hide');
            },
            rules: {
                name: {
                    required: true,
                    minlength: 2,
                },
                quota: {
                    required: true,
                    number: true,
                    min: 1,
                    max: 1023
                }
            }
        });

        if($(parent_id + ' form').valid())
        {
            Core.modules.Helper.popup('show');
            
            var company_id = $(parent_id).find('input[type="hidden"][name="id"]').val();
            var quota_size = $(parent_id + ' form').find('input[type="text"][name="quota"]').val();
            var quota_type = $(parent_id + ' form').find('select[name="quota_type"] option:selected').text();
 
            var quota = Core.modules.Helper.convertToBytes(quota_size, quota_type);

            var data = {
                'company_id':   $(parent_id).find('input[type="hidden"][name="id"]').val(),
                'name':         $(parent_id + ' form').find('input[type="text"][name="name"]').val(),
                'quota':        quota,
            }
            
            var req = Core.modules.Helper.ajax('PUT', mainConfig[Core.moduleName].apiSource + company_id, JSON.stringify(data));
            req.done(function(msg){
                Core.modules.Helper.showDialog('Company', 'Company edit successfully', 1);
                _self.run();
            });
            req.fail(function(error){
                console.log(error);	
            });
            
            Core.modules.Helper.popup();
        }
    },

    deleteCompany: function(element){
        var _self = this;
        var parent_id = '#' + $(element).parent()[0].id;
        var company_id = $(parent_id).find('input[type="hidden"][name="id"]').val();

        var req = Core.modules.Helper.ajax('DELETE', mainConfig[Core.moduleName].apiSource + company_id);
        req.done(function(msg){
            _self.run();
        });
        req.fail(function(error){
        });
    },


    prepareDialog: function(){
        var _self = this;
        var formDiv = $('<div/>', {id : 'addDialog', style: 'display: none', title: 'ADD COMPANY'});
        formDiv.load(Core.templatePath + 'Companies/addCompany.html', function(){

        });
        $('#dialogBox').append(formDiv);

        $.each(information_unit, function(ind, type){
            $('.bootstrap-frm').find('select[name="quota_type"]')
                .append($("<option></option>")
                    .attr("value", ind)
                    .text(type));

        });

        $( "#addDialog" ).dialog({
            width: 330,
            height: 345,
            position: { at: "top top+10%", of: window},
            open: function()
            {
                Core.modules.Helper.popup('show');
            },
            close: function(event, ui)
            {
                $('#addDialog').remove();
            },
            beforeClose: function() {Core.modules.Helper.popup('close');}
        });
        
        $('#opaco').click(function(){
            Core.modules.Helper.closeDialog();
        });
    },

    addCompany: function(){
        var _self = this;
        $('.bootstrap-frm input[type="text"]').tooltipster({
            trigger: 'custom',
            onlyOne: false,
            position: 'right',
            theme: 'tooltipster-shadow'
        });
        $(".bootstrap-frm").validate({
            errorPlacement: function (error, element) {
                $(element).tooltipster('update', $(error).text());
                $(element).tooltipster('show');
            },
            success: function (label, element) {
                $(element).tooltipster('hide');
            },
            rules: {
               name: {
                    required: true,
                    minlength: 2,
                },
                quota: {
                    required: true,
                    number: true,
                    min: 1,
                    max: 1023
                }
            }
        });

        if($(".bootstrap-frm").valid()){
            var quota_size = $(".bootstrap-frm input[name='quota']").val();
            var quota_type = $(".bootstrap-frm select[name='quota_type'] option:selected").text();
 
            var quota = Core.modules.Helper.convertToBytes(quota_size, quota_type);

            var data = {
                'name':         $(".bootstrap-frm input[name='name']").val(),
                'quota':        quota,
            }

            var req = Core.modules.Helper.ajax('POST',  mainConfig[Core.moduleName].apiSource, JSON.stringify(data));
            req.done(function(msg){
                Core.modules.Helper.showDialog('Company', 'Company create successfully', 1);
                Core.modules.Helper.closeDialog();
                _self.run();
            });
            req.fail(function(msg){
                Core.modules.Helper.popup('show');
                Core.modules.Helper.showDialog('Company', msg.responseJSON.errors.message, 1);
            });
        }
    },
};