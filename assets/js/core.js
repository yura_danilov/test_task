var Core = {
    includedModules: [],
    defaultModule: '',
    accessoryModules: ['Companies'],
    modulePath: window.location.href + 'assets/js/modules/',
    templatePath: window.location.href + 'assets/js/templates/',

    init: function(module){
        var _self = this;
        
        $.ajaxSetup ({
            // Disable caching of AJAX responses
            cache: false,
            async:false,
            global: true,
            contentType: 'application/json'
        });
        
        var src = _self.modulePath + module  + '.js';
        if ($.inArray(module, _self.includedModules) == 0){
            _self.modules[module].init();
        }
        else{
            $.getScript(src, function(){
                _self.includedModules.push(module);
                _self.modules[module].init();
            });
        }
    },

    run: function(module){
        var _self = this;

        _self.moduleName = module;
        var src = _self.modulePath + module  + '.js';
        if ($.inArray(module, _self.includedModules) == 0){
            _self.modules[module].run();
        }
        else{
            $.getScript(src, function(){
                _self.includedModules.push(module);
                _self.modules[module].run();
            });
        }
    }
};

Core.modules = {};
