<?php

use Doctrine\Common\ClassLoader,
	Doctrine\ORM\Tools\Setup,
	Doctrine\ORM\EntityManager;

class Doctrine
{
	public $em;

	public function __construct()
	{
		require_once __DIR__ . '/Doctrine/ORM/Tools/Setup.php';
		Setup::registerAutoloadDirectory(__DIR__);

		// Load the database configuration from CodeIgniter
		require APPPATH . 'config/database.php';
		$active_group = 'default';
		if (defined('CIUnit_Version'))
		{
			$active_group .= '_test';
		}
                
                
                /* Connection settings for other drivers
		        'driver'		=> 'pdo_mysql',
        		'driver'		=> 'pdo_pgsql'
	        	'port'                  => $db['default']['port'],
                        'driver'                => 'pdo_pgsql',
                        'user'                  => $db[$active_group]['username'],
                        'password'              => $db[$active_group]['password'],
                        'host'                  => $db[$active_group]['hostname'],
                        'dbname'                => $db[$active_group]['database'],
                */

		$connection_options = array(
			'driver'		=> 'pdo_mysql',
			'dsn'			=> $db['default']['dsn'],
                        'dbname'                => $db['default']['database'],
                        'user'                  => $db['default']['username'],
                        'password'              => $db['default']['password'],
			'charset'		=> $db['default']['char_set'],
			'driverOptions'	=> array(
				'charset'	=> $db['default']['char_set'],
			),
		);

		// With this configuration, your model files need to be in application/models/Entity
		// e.g. Creating a new Entity\User loads the class from application/models/Entity/User.php
		$models_namespace = 'Entity';
		$models_path = APPPATH . 'models';
		$proxies_dir = APPPATH . 'models/proxies';
		$metadata_paths = array(APPPATH . 'models/Entity');

		// Set $dev_mode to TRUE to disable caching while you develop
		$dev_mode = true;

		// If you want to use a different metadata driver, change createAnnotationMetadataConfiguration
		// to createXMLMetadataConfiguration or createYAMLMetadataConfiguration.
		$config = Setup::createAnnotationMetadataConfiguration($metadata_paths, $dev_mode, $proxies_dir, null, false);
		$this->em = EntityManager::create($connection_options, $config);

		$loader = new ClassLoader($models_namespace, $models_path);
		$loader->register();
	}

}
