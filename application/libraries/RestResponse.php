<?php

class RestResponse
{
	const COMPLETE = 'complete';
	const ERROR = 'error';
	private $status;
	private $message;
	private $data;

	public function __construct()
	{
		$this->status = 'complete';
		$this->message = '';
		$this->data = array();
	}

	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getStatus()
	{
		return $this->status;
	}

	public function setMessage($message)
	{
		$this->message = $message;
	}

	public function setError($error)
	{
		$this->status = 'error';
		$this->message = $error;
	}

	public function setData($data)
	{
		$this->data = $data;
	}

	public function addData($key, $value)
	{
		$this->data[$key] = $value;
	}

	public function asArray()
	{
		return array(
			'status' => $this->status,
			'message' => $this->message,
			'data' => $this->data
		);
	}

	public function isError()
	{
		return $this->status === 'error';
	}
}
