<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Test task</title>
        
        <link rel="stylesheet" href="<? echo base_url();?>assets/css/bootstrap_3_3_7/bootstrap.min.css">
        <link rel="stylesheet" href="<? echo base_url();?>assets/css/bootstrap_3_3_7/bootstrap-theme.min.css">
        <link rel="stylesheet" href="<? echo base_url();?>assets/css/jquery-ui.min.css">
        <link rel="stylesheet" href="<? echo base_url();?>assets/css/tooltipster/tooltipster.css">
        <link rel="stylesheet" href="<? echo base_url();?>assets/css/tooltipster/themes/tooltipster-shadow.css">
        <link rel="stylesheet" href="<? echo base_url();?>assets/css/default.css">

        <script src="<? echo base_url();?>assets/js/jquery-3.1.1.min.js"></script>
        <script src="<? echo base_url();?>assets/js/bootstrap_3_3_7/bootstrap.min.js"></script>
        <script src="<? echo base_url();?>assets/js/jquery-ui.min.js"></script>
        <script src="<? echo base_url();?>assets/js/jquery.validate.js"></script>
        <script src="<? echo base_url();?>assets/js/jquery.tooltipster.min.js"></script>
        <script src="<? echo base_url();?>assets/js/config.js"></script>
        <script src="<? echo base_url();?>assets/js/core.js"></script>
        <script src="<? echo base_url();?>assets/js/modules/Helper.js"></script>
        <script src="<? echo base_url();?>assets/js/modules/DataGenerator.js"></script>
        
        <script>
	$(function() {
            $.each(Core.accessoryModules, function(index, module){
                Core.init(module);  
            });
            
            $('.menu_links').click(function(event) {
                if( "Data" != event.target.title){
                    Core.run(event.target.title);
                }
            });
        });
        </script>
</head>
<body>

<div id="opaco" class="hidden"></div>
<div id="popup" class="hidden"></div>
<div id="dialogBox" class="clear"></div>
<div id="container">
    <div id="body">
        <div id="exTab" class="container">	
            <ul class="nav nav-pills">
                <li class="active">
                    <a  href="#1b" title="Data" class="menu_links" data-toggle="tab">Data</a>
                </li>
                <li>
                    <a  href="#2b" title="Users" class="menu_links" data-toggle="tab">Users</a>
                </li>
                <li>
                    <a href="#3b" title="Companies" class="menu_links" data-toggle="tab">Companies</a>
                </li>
                <li>
                    <a href="#4b" title="TransferLogs" class="menu_links" data-toggle="tab">Transfer logs</a>
                </li>
            </ul>

            <div class="tab-content clearfix">
                 <div class="tab-pane active" id="1b">
                     <div id="data_table">
                         <input class="btn btn-primary" type="button" value="Generate Random Data" onclick="Core.modules.DataGenerator.run()" />
                    </div>
                </div>
                <div class="tab-pane" id="2b">
                    <div id="data_table">
                         <input class="btn btn-primary" type="button" value="Add User" onclick="Core.modules.Users.prepareDialog()" />
                    </div>
                    <div id="users_table"></div>
                </div>
                <div class="tab-pane" id="3b">
                    <div id="data_table">
                         <input class="btn btn-primary" type="button" value="Add Company" onclick="Core.modules.Companies.prepareDialog()" />
                     </div>
                    <div id="companies_table"></div>
                </div>
                <div class="tab-pane" id="4b">
                    <div>
                        <div class="oprion_tab">
                            <label for="monthspicker">Date :</label>
                            <select id="monthspicker" class="form-control"></select>
                            <input class="btn btn-primary" type="button" value="Get report" onclick="Core.modules.TransferLogs.getReport()" />
                        </div>
                    </div>
                    <div class="clear"></div>
                    <div id="transfer_logs_table"></div>
                </div>
            </div>
        </div>
    </div>

    <p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</div>

</body>
</html>