<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Carbon\Carbon;

class DataGenerator extends CI_Model {

    private $em;

    public function __construct()
    {
        parent::__construct();

        $this->load->library('doctrine');
        $this->em = $this->doctrine->em;
        $this->load->model('users');
        $this->load->model('companies');
        $this->load->model('transfers');
    }

    public function genereteRandomData()
    {
        $bytes = array(
            1 => 1024,
            2 => 1048576,
            3 => 1073741824,
            4 => 1099511627776
        );

        $users_quantity = mt_rand(50, 100);
        $company_quantity = mt_rand(50, 80);

        $faker = Faker\Factory::create();

        for( $cp = 0; $cp < $company_quantity; $cp++)
        {
            $company_quota_limit = mt_rand(100 , 1000) * $bytes[mt_rand(2 , 4)];
            $company_name = $faker->company;

            if( !$this->companies->checkCompany($company_name) )
            {
                $this->companies->addCopmany($company_name, $company_quota_limit);
            }                
        }

        $companies = $this->em->getRepository('Entity\Companies')->findAll();
        $companies_count = count($companies) - 1;

        for( $i = 0; $i < $users_quantity; $i++ )
        {
            $user_name = $faker->firstName . ' ' . $faker->lastName;
            $user_email = $faker->safeEmail;

            if( !$this->users->checkUser($user_name) )
            {
                $company_random = $companies[mt_rand(0, $companies_count)];
                $user_id = $this->users->addUser($user_name, $user_email, $company_random);

                $day_rand = mt_rand(0, 30);
                $month_rand = mt_rand(0, 6);
                $transfers_amount = mt_rand(2,6);
                $transfers_bytes = mt_rand(100 , 1024) * $bytes[mt_rand(1 , 3)];


                if($month_rand == 0)
                {
                    $day_rand = mt_rand(0, Carbon::now()->day);
                    $date = Carbon::parse('-' . $day_rand . ' days')->toDateTimeString();
                    $date = Carbon::createFromFormat('Y-m-d H:i:s', $date);
                }
                else
                {
                    $date = Carbon::parse('-' . $month_rand . 'months ' . '-' . $day_rand . ' days')->toDateTimeString();
                    $date = Carbon::createFromFormat('Y-m-d H:i:s', $date);
                }

                for( $t = 0; $t < $transfers_amount; $t++)
                {
                    $this->transfers->addTransfer($user_id, $date, $faker->url, $transfers_bytes);
                }                    

            }
        }
    }
}
