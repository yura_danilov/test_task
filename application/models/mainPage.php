<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MainPage extends CI_Model {

    private $em;
    
    public function __construct()
    {
            parent::__construct();
            $this->load->library('doctrine');
            $this->em = $this->doctrine->em;
            $this->load->helper('url');
    }

    public function index()
    {
        try {
            $this->em->getConnection()->connect();
            
            $schemaManager = $this->em->getConnection()->getSchemaManager();
            $classes = $this->em->getMetadataFactory()->getAllMetadata();
            foreach ($classes as $value)
            {
                if( !$schemaManager->tablesExist(array($value->table['name'])) )
                {
                    return 'configure';
                }
            }
            return 'adjusted';
        } catch (\Exception $e) {
             return 'configure';
        }
    }
}
