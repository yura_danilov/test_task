<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Model {

    private $em;
    protected $answer = array();


    public function __construct()
    {
        parent::__construct();

        $this->load->library('doctrine');
        $this->em = $this->doctrine->em;
    }

    public function getAllUsers()
    {
        $users = $this->em->getRepository('Entity\Users')->getAll();
        
        if(is_array($users) && count($users) > 0 ){
            $this->answer['body'] = $users;
            $this->answer['code'] = 200;
            return $this->answer;
        }
        elseif ( is_array($users) && count($users) == 0  ){
            $this->answer['body'] = 'No users';
            $this->answer['code'] = 200;
            return $this->answer;
        }
        else{
            $this->answer['body']['errors'] = $users;
            $this->answer['code'] = 404;
            return $this->answer;
        }
    }
        
    public function checkUser($name)
    {
        return $this->em->getRepository('Entity\Users')->findOneBy(['name' => $name]);
    }
        
    public function addUser($name, $email, $company_id)
    {
        $company_reference = $this->em->getRepository('Entity\Companies')->findOneBy(array('id' => $company_id));
        
        if(is_null($company_reference) ){
            $this->answer['body']['errors']['message'] = 'Company id not found';
            $this->answer['code'] = 404;
            return $this->answer;
        }
        
        $email_exists = $this->em->getRepository('Entity\Users')->findOneBy(array('email' => $email));
        
        if( !is_null($email_exists) ){
            $this->answer['body']['errors']['message'] = 'Email (' . $email . ') use by another user';
            $this->answer['code'] = 404;
            return $this->answer;
        }
        
        $user = new Entity\Users;
        $user->setName($name);
        $user->setEmail($email);
        $user->setCompany($company_reference);
        
        try
        {
            $this->em->persist($user);
            $this->em->flush();
            return $user->getId();
        }
        catch (\Exception $e)
        {
            if( 'development' == ENVIRONMENT )
            {
                $this->answer['body']['errors']['message'] = $e->getMessage();
                $this->answer['code'] = 404;
                return $this->answer;
            } else {
                $this->answer['body']['errors']['message'] = 'Add user fail';
                $this->answer['code'] = 404;
                return $this->answer;
            }
        }
    }
        
    public function updateUser($user_id, $name, $email, $company_id)
    {        
        $user = $this->em->getRepository('Entity\Users')->findOneBy(array('id' => $user_id));

        if(is_null($user) ){
            $this->answer['body']['errors']['id'] = 'Not found';
            $this->answer['code'] = 404;
            return $this->answer;
        }

        $email_exists = $this->em->getRepository('Entity\Users')->findOneBy(array('email' => $email));
        
        if( !is_null($email_exists) ){
            if( $email_exists->getId() != $user->getId()){
                $this->answer['body']['errors']['message'] = 'Email (' . $email . ') use by another user';
                $this->answer['code'] = 404;
                return $this->answer;
            }
        }
        
        $company_reference = $this->em->getRepository('Entity\Companies')->findOneBy(array('id' => $company_id));
        
        if(is_null($company_reference) ){
            $this->answer['body']['errors']['message'] = 'Company not found';
            $this->answer['code'] = 404;
            return $this->answer;
        }
        
        $user->setName($name);  
        $user->setEmail($email);
        $user->setCompany($company_reference);

        $this->answer['body']['id'] = $user_id;
        $this->answer['body']['name'] = $name;
        $this->answer['body']['email'] = $email;
        $this->answer['body']['company_id'] = $company_reference->getId();
        
        try
        {
            $this->em->persist($user);
            $this->em->flush();
            
            $this->answer['code'] = 200;
            return $this->answer;
        }
        catch (\Exception $e)
        {
           if( 'development' == ENVIRONMENT )
           {
                $this->answer['body']['errors']['SQL'] = $e->getMessage();
                $this->answer['code'] = 404;
                return $this->answer;

           } else {
                $this->answer['body']['errors']['SQL'] = 'Update fail';
                $this->answer['code'] = 404;
                return $this->answer;
           }
        }
    }

    public function deleteUser($user_id)
    {
        $user = $this->em->getRepository('Entity\Users')->findOneBy(array('id' => $user_id));

        if(is_null($user) ){
            $this->answer['body']['errors']['id'] = 'Not found';
            $this->answer['code'] = 404;
            return $this->answer;
        }

        try
        {
            $this->em->remove($user);
            $this->em->flush();
            
            $this->answer['body'] = null;
            $this->answer['code'] = 200;
            return $this->answer;
        }
        catch (\Exception $e)
        {
            if( 'development' == ENVIRONMENT )
            {
                 $this->answer['body']['errors']['SQL'] = $e->getMessage();
                 $this->answer['code'] = 404;
                 return $this->answer;

            } else {
                 $this->answer['body']['errors']['SQL'] = 'Delete fail';
                 $this->answer['code'] = 404;
                 return $this->answer;
            }
        }
    }
}
