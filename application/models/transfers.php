<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transfers extends CI_Model {

    private $em;
    protected $answer = array();


    public function __construct()
    {
        parent::__construct();

        $this->load->library('doctrine');
        $this->em = $this->doctrine->em;
    }
        
    public function addTransfer($user_id, $date, $resource, $bytes)
    {
        $user = $this->em->getRepository('Entity\Users')->findOneBy(['id' => $user_id]);
        
        $transfer = new Entity\TransferLogs;
        $transfer->setUser($user);
        $transfer->setDateTime($date);
        $transfer->setResource($resource);
        $transfer->setTransferedBytes($bytes);

        try
        {
            $this->em->persist($transfer);
            $this->em->flush();
        }
        catch (\Exception $e)
        {
            if( 'development' == ENVIRONMENT )
            {
                return $e->getMessage();
            } else {
                return 'Bad query';
            }
        }
    }
}
