<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Carbon\Carbon;

class Report extends CI_Model {

    private $em;
    protected $answer = array();


    public function __construct()
    {
        parent::__construct();

        $this->load->library('doctrine');
        $this->em = $this->doctrine->em;
    }

    public function getReport($month)
    {
        $dt = Carbon::create(null, $month, 1);
        $starDate = $dt->startOfMonth()->format('Y-m-d H:i:s');
        $endDate = $dt->endOfMonth()->format('Y-m-d H:i:s');
        
        $report = $this->em->getRepository('Entity\TransferLogs')->getCompaniesExceededLimit($starDate, $endDate);
        
        if(is_array($report) && count($report) > 0 ){
            $this->answer['body'] = $report;
            $this->answer['code'] = 200;
            return $this->answer;
        }
        elseif ( is_array($report) && count($report) == 0  ){
            $this->answer['body'] = 'No data';
            $this->answer['code'] = 200;
            return $this->answer;
        }
        else{
            $this->answer['body']['errors'] = $report;
            $this->answer['code'] = 404;
            return $this->answer;
        }
    }
}
