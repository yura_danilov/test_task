<?php

namespace Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TransferLogs
 * @ORM\Entity(repositoryClass="Entity\Repositories\TransferLogsRepository")
 * @ORM\Table(name="transfer_logs", indexes={@ORM\Index(name="FK_transfer_logs_users", columns={"user_id"})})
 */
class TransferLogs
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_time", type="datetime", nullable=false)
     */
    protected $date_time;

    /**
     * @var string
     *
     * @ORM\Column(name="resource", type="string", length=2083, nullable=false)
     */
    protected $resource;

    /**
     * @var integer
     *
     * @ORM\Column(name="transfered_bytes", type="bigint", nullable=false)
     */
    protected $transfered_bytes;

    /**
     * @var \Users
     *
     * @ORM\ManyToOne(targetEntity="Users")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=FALSE, onDelete="CASCADE")
     * 
     */
    protected $user_id;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateTime
     *
     * @param \DateTime $dateTime
     *
     * @return TransferLogs
     */
    public function setDateTime($date_time)
    {
        $this->date_time =  $date_time;

        return $this;
    }

    /**
     * Get dateTime
     *
     * @return \DateTime
     */
    public function getDateTime()
    {
        return $this->date_time;
    }

    /**
     * Set resource
     *
     * @param string $resource
     *
     * @return TransferLogs
     */
    public function setResource($resource)
    {
        $this->resource = $resource;

        return $this;
    }

    /**
     * Get resource
     *
     * @return string
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * Set transfered_bytes
     *
     * @param integer $transfered_bytes
     *
     * @return TransferLogs
     */
    public function setTransferedBytes($transfered_bytes)
    {
        $this->transfered_bytes = $transfered_bytes;

        return $this;
    }

    /**
     * Get transfered_bytes
     *
     * @return integer
     */
    public function getTransferedBytes()
    {
        return $this->transfered_bytes;
    }

    /**
     * Set user_id
     *
     * @param \Users $user
     *
     * @return TransferLogs
     */
    public function setUser(Users $user = null)
//    public function setUser($user)
    {
        $this->user_id = $user;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return \Users
     */
    public function getUser()
    {
        return $this->user_id;
    }
}
