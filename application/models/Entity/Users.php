<?php

namespace Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Users
 *
 * @ORM\Entity(repositoryClass="Entity\Repositories\UsersRepository")
 * @ORM\Table(name="users", indexes={@ORM\Index(name="FK_users_companies", columns={"company_id"})})
 */
class Users
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=50, nullable=false)
     */
    protected $email;

    /**
     * @var \Companies
     *
     * @ORM\ManyToOne(targetEntity="Companies", inversedBy="users")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id", nullable=FALSE, onDelete="CASCADE")
     */
    protected $company_id;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Users
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Users
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set company
     *
     * @param \Companies $company
     *
     * @return Users
     */
    public function setCompany(Companies $company = null)
    {
        $this->company_id = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \Companies
     */
    public function getCompany()
    {
        return $this->company_id;
    }
}
