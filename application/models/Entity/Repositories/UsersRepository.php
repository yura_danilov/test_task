<?php
namespace Entity\Repositories;

use Doctrine\ORM\EntityRepository;
use Entity;

class UsersRepository extends EntityRepository {
    private $default_order = 'DESC';
    private $default_limit_from = 0;

    public function getAll()
    {
        $qb = $this->_em->createQueryBuilder();

        try
        {
            $res = $qb->select(array(
                        'ur.id as id',
                        'ur.name as name', 
                        'ur.email as email', 
                        'cp.name as company'
                    ))
                    ->from('Entity\Users', 'ur')
                    ->leftJoin('Entity\Companies', 'cp', \Doctrine\ORM\Query\Expr\Join::WITH, 'ur.company_id = cp.id')
                    ->orderBy('ur.name', 'ASC')
                    ->getQuery()
                    ->getArrayResult();

            return $res;
        }
        catch (\Exception $e)
        {
            if( 'development' == ENVIRONMENT )
            {
                return $e->getMessage();
            } else {
                return 'Bad query';
            }
        }
    }
}
