<?php
namespace Entity\Repositories;

use Doctrine\ORM\EntityRepository;
use Entity;

class CompaniesRepository extends EntityRepository {
    private $default_order = 'DESC';
    private $default_limit_from = 0;

    public function getAll()
    {
        $qb = $this->_em->createQueryBuilder();

        try
        {
            $res = $qb->select('cp')
                    ->from('Entity\Companies', 'cp')
                    ->orderBy('cp.name', 'ASC')
                    ->getQuery()
                    ->getArrayResult();

            return $res;
        }
        catch (\Exception $e)
        {
            if( 'development' == ENVIRONMENT )
            {
                return $e->getMessage();
            } else {
                return 'Bad query';
            }
        }
    }
}
