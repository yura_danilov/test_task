<?php
namespace Entity\Repositories;

use Doctrine\ORM\EntityRepository;
use Entity;

class TransferLogsRepository extends EntityRepository {
    private $default_order = 'DESC';
    private $default_limit_from = 0;

    public function getCompaniesExceededLimit($starDate, $endDate)
    {
        $qb = $this->_em->createQueryBuilder();

        try
        {
            $res = $qb->select(array(
                        'cp.id',
                        'cp.name',
                        'sum(tl.transfered_bytes) as over_quota', 
                        'cp.quota as quota'
                    ))
                    ->from('Entity\TransferLogs', 'tl')
                    ->leftJoin('Entity\Users', 'ur', \Doctrine\ORM\Query\Expr\Join::WITH, 'tl.user_id = ur.id')
                    ->leftJoin('Entity\Companies', 'cp', \Doctrine\ORM\Query\Expr\Join::WITH, 'ur.company_id = cp.id')
                    ->where('tl.date_time BETWEEN :start AND :end')
                    ->setParameter('start', $starDate)
                    ->setParameter('end', $endDate)
                    ->groupBy('cp.name, cp.quota')
                    ->having('over_quota > quota')
                    ->orderBy('cp.name', 'ASC')
                    ->getQuery()
                    ->getArrayResult();
//                    ->get
            
            return $res;
        }
        catch (\Exception $e)
        {
            if( 'development' == ENVIRONMENT )
            {
                return $e->getMessage();
            } else {
                return 'Bad query';
            }
        }
    }
}
