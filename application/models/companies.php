<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Companies extends CI_Model {

    private $em;
    protected $answer = array();

    public function __construct()
    {
        parent::__construct();

        $this->load->library('doctrine');
        $this->em = $this->doctrine->em;
    }

    public function getAllCompanies()
    {
        $companies = $this->em->getRepository('Entity\Companies')->getAll();
        
        if(is_array($companies) && count($companies) > 0 ){
            $this->answer['body'] = $companies;
            $this->answer['code'] = 200;
            return $this->answer;
        }
        elseif( is_array($companies) && count($companies) == 0  ){
            $this->answer['body'] = 'No companies';
            $this->answer['code'] = 200;
            return $this->answer;
        }
        else{
            $this->answer['body']['errors'] = $companies;
            $this->answer['code'] = 404;
            return $this->answer;
        }
    }
        
    public function checkCompany($name)
    {
        return $this->em->getRepository('Entity\Companies')->findOneBy(['name' => $name]);
    }
        
    public function updateCompany($company_id, $name, $quota)
    {
        $company = $this->em->getRepository('Entity\Companies')->findOneBy(array('id' => $company_id));
        
        if(is_null($company) ){
            $this->answer['body']['errors']['id'] = 'Not found';
            $this->answer['code'] = 404;
            return $this->answer;
        }
        
        $company->setName($name);  
        $company->setQuota($quota);
      
        $this->answer['body']['id'] = $company_id;
        $this->answer['body']['name'] = $name;
        $this->answer['body']['quota'] = $quota;
        
        try
        {
            $this->em->persist($company);
            $this->em->flush();
            
            $this->answer['code'] = 200;
            return $this->answer;
        }
        catch (\Exception $e)
        {
           if( 'development' == ENVIRONMENT )
           {
                $this->answer['body']['errors']['SQL'] = $e->getMessage();
                $this->answer['code'] = 404;
                return $this->answer;

           } else {
                $this->answer['body']['errors']['SQL'] = 'Update fail';
                $this->answer['code'] = 404;
                return $this->answer;
           }
        }
    }

    public function deleteCompany($company_id)
    {
        $company = $this->em->getRepository('Entity\Companies')->findOneBy(array('id' => $company_id));

        if(is_null($company) ){
            $this->answer['body']['errors']['id'] = 'Not found';
            $this->answer['code'] = 404;
            return $this->answer;
        }

        try
        {
            $this->em->remove($company);
            $this->em->flush();
            
            $this->answer['body'] = null;
            $this->answer['code'] = 200;
            return $this->answer;
        }
        catch (\Exception $e)
        {
            if( 'development' == ENVIRONMENT )
            {
                 $this->answer['body']['errors']['SQL'] = $e->getMessage();
                 $this->answer['code'] = 404;
                 return $this->answer;

            } else {
                 $this->answer['body']['errors']['SQL'] = 'Delete fail';
                 $this->answer['code'] = 404;
                 return $this->answer;
            }
        }
    }
	
    public function addCopmany($name, $quota_limit)
    {
         $company_name_exists = $this->em->getRepository('Entity\Companies')->findOneBy(array('name' => $name));
        
        if( !is_null($company_name_exists) ){
            $this->answer['body']['errors']['message'] = 'Company (' . $name . ') already exist';
            $this->answer['code'] = 404;
            return $this->answer;
        }
        
        $company = new Entity\Companies;
        $company->setName($name);
        $company->setQuota($quota_limit);

        try
        {
            $this->em->persist($company);
            $this->em->flush();
            return $company->getId();
        }
        catch (\Exception $e)
        {
            if( 'development' == ENVIRONMENT )
            {
                $this->answer['body']['errors']['message'] = $e->getMessage();
                $this->answer['code'] = 404;
                return $this->answer;
            } else {
                $this->answer['body']['errors']['message'] = 'Add company fail';
                $this->answer['code'] = 404;
                return $this->answer;
            }
        }
    }
}
