<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Install extends CI_Controller {
    protected $em;


    public function __construct()
    {
        parent::__construct();
        $this->load->model('mainPage');
        $this->load->library('doctrine');
        $this->em = $this->doctrine->em;
        $this->load->helper('url');
    }
    
    public function index()
    {
        $act = $this->mainPage->index();
        switch ($act) {
            case 'configure':
                try 
                {
                    $schemaTool = new \Doctrine\ORM\Tools\SchemaTool($this->em);
                    $schemaManager = $this->em->getConnection()->getSchemaManager();
                    $classes = $this->em->getMetadataFactory()->getAllMetadata();
                    foreach ($classes as $key => $value)
                    {
                        if( $schemaManager->tablesExist(array($value->table['name'])) )
                        {
                           unset($classes[$key]);
                        }
                    }
                    $schemaTool->createSchema($classes);
                    
                    redirect(base_url());
                } 
                catch (Exception $e)
                {
                    redirect(base_url());
                }
                
                break;
            default:
                break;
        }
    }
}

/* End of file install.php */
/* Location: ./application/controllers/install.php */
