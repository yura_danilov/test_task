<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

    public function index()
    {
        $this->load->model('mainPage');
        
        $act = $this->mainPage->index();
        
        switch ($act) {
            case 'configure':
                $this->load->view('main_template');
                break;
            case 'adjusted':
                $this->load->view('index_template');
            default:
                break;
        }
    }
}
