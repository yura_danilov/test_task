<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Api extends REST_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->library('RestResponse');             
    }

    public function index_get()
    {
        $this->load->view('api_template');
    }

    public function report_get($month = false)
    {
        $this->load->model('report');
        
        if($month)
        {
            $answer = $this->report->getReport($month);
            $this->response($answer['body'], $answer['code']);
        }
        else
        {
            $answer = array( 
                'body' => array(
                    'errors'    => ["month" => "Not set"]), 
                'code' => 404);
            $this->response($answer['body'], $answer['code']);
        }
    }
    
    public function generator_post()
    {
        $this->load->model('dataGenerator');
        
        $this->dataGenerator->genereteRandomData();
        
        $this->response(null, 200);
    }
    
    public function users_get()
    {
        $this->load->model('users');

        $answer = $this->users->getAllUsers();

        $this->response($answer['body'], $answer['code']);
    }
    
    public function users_put($id = false)
    {
        $this->load->model('users');
        
        $user_data = json_decode(@file_get_contents('php://input'));

        if($id)
        {
            $answer = $this->users->updateUser($id, $user_data->name, $user_data->email, $user_data->company_id);
            $this->response($answer['body'], $answer['code']);
        }
        else
        {
            $answer = array( 
                'body' => array(
                    'errors'    => ["id" => "Not set"]), 
                'code' => 404);
            $this->response($answer['body'], $answer['code']);
        }
    }
    
    public function users_delete($id = false)
    {
        $this->load->model('users');

        if($id)
        {
            $answer = $this->users->deleteUser($id);
            $this->response($answer['body'], $answer['code']);
        }
        else
        {
            $answer = array( 
                'body' => array(
                    'errors'    => ["id" => "Not set"]), 
                'code' => 404);
            $this->response($answer['body'], $answer['code']);
        }
    }
    
    public function users_post()
    {
        $this->load->model('users');

        $user_data = json_decode(@file_get_contents('php://input'));
        
        $answer = $this->users->addUser($user_data->name, $user_data->email, $user_data->company_id);
        if(is_numeric($answer))
        {
            $answer = array( 
                'body' => array(
                    array(
                        "id" => $answer,
                        'name' => $user_data->name,
                        'email' => $user_data->email,
                        'company_id' => $user_data->company_id)), 
                'code' => 201);
            $this->response($answer['body'], $answer['code']);
        }
        else
        {
            
        }
        $this->response($answer['body'], $answer['code']);
    }

    public function companies_get()
    {
        $this->load->model('companies');

        $answer = $this->companies->getAllCompanies();
        $this->response($answer['body'], $answer['code']);
    }
    
    public function companies_put($id = false)
    {
        $this->load->model('companies');
        
        $company_data = json_decode(@file_get_contents('php://input'));

        if($id)
        {
            $answer = $this->companies->updateCompany($id, $company_data->name, $company_data->quota);
            $this->response($answer['body'], $answer['code']);
        }
        else
        {
            $answer = array( 
                'body' => array(
                    'errors'    => ["id" => "Not set"]), 
                'code' => 404);
            $this->response($answer['body'], $answer['code']);
        }
    }
    
    public function companies_delete($id = false)
    {
        $this->load->model('companies');

        if($id)
        {
            $answer = $this->companies->deleteCompany($id);
            $this->response($answer['body'], $answer['code']);
        }
        else
        {
            $answer = array( 
                'body' => array(
                    'errors'    => ["id" => "Not set"]), 
                'code' => 404);
            $this->response($answer['body'], $answer['code']);
        }
    }
    
    public function companies_post()
    {
        $this->load->model('companies');

        $company_data = json_decode(@file_get_contents('php://input'));
        
        $answer = $this->companies->addCopmany($company_data->name, $company_data->quota);
        if(is_numeric($answer))
        {
            $answer = array( 
                'body' => array(
                    array(
                        "id" => $answer,
                        'name' => $company_data->name,
                        'quota' => $company_data->quota)), 
                'code' => 201);
            $this->response($answer['body'], $answer['code']);
        }
        else
        {
            
        }
        $this->response($answer['body'], $answer['code']);
    }
}
